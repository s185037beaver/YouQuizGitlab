import { FC } from "react";
import LoginForm from "./LoginForm";

import { Box, SimpleGrid } from "@chakra-ui/layout";

import DividerWithText from "./DividerWithText";
import { GoogleLogin } from "react-google-login";
import { AuthStore } from "../../stores/authStore";
import { useToast } from "@chakra-ui/toast";
import { useHistory } from "react-router";

export const LoginCard: FC = () => {
  const history = useHistory();

  const successResponse = (response: any) => {
    AuthStore.googleLogin(response.tokenId)
      .then(() => {
        history.push("/");
      })
      .catch((err: Error) => {
        toast({
          title: `${err}`,
          status: "error",
          isClosable: true,
        });
      });
  };

  const toast = useToast();

  const failureResponse = (response: any) => {
    toast({
      title: `${response}`,
      status: "error",
      isClosable: true,
    });
  };

  return (
    <>
      <Box p="6" rounded="md" mt="6" maxW="md" mx="auto">
        <LoginForm />
        <DividerWithText>or continue with</DividerWithText>
        <SimpleGrid mt="6" columns={1} spacing="3">
          <GoogleLogin
            clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID as string}
            buttonText="Login"
            onSuccess={successResponse}
            onFailure={failureResponse}
            cookiePolicy={"single_host_origin"}
          />
        </SimpleGrid>
      </Box>
    </>
  );
};
