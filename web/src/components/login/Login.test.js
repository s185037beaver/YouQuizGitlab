import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import LoginPage from "../../pages/LoginPage";
import { HashRouter } from "react-router-dom";
import LoginForm from "./LoginForm";

describe("LoginComponentTest", () => {
  test("Toggle between sign in and sign up", async () => {
    render(
      <HashRouter>
        <LoginPage />
      </HashRouter>
    );
    expect(screen.getByText("Sign in to your account")).toBeInTheDocument();
    fireEvent.click(screen.getByText("Sign up!"));
    await waitFor(() => {
      expect(screen.getByText("Sign in here!")).toBeInTheDocument();
    });
  });

  test("Check successful login", async () => {
    render(<LoginForm />);

    fireEvent.change(screen.getByTestId("email"), {
      target: { value: "test@test.dk" },
    });
    fireEvent.change(screen.getByTestId("password"), {
      target: { value: "1234" },
    });

    await waitFor(() => {
      fireEvent.click(screen.getByTestId("sign-in"));
      expect(localStorage.getItem("access_token")).toBe("testToken");
    });
  });

  test("Check failure login", async () => {
    localStorage.setItem("access_token", "") // Clearing acces in LS frem previous test
    render(<LoginForm />);

    fireEvent.change(screen.getByTestId("email"), {
      target: { value: "test@test.dk" },
    });
    fireEvent.change(screen.getByTestId("password"), {
      target: { value: "wrongPassword" },
    });

    await waitFor(() => {
      fireEvent.click(screen.getByTestId("sign-in"));
      expect(localStorage.getItem("access_token")).toBe("");
    });
  });

  test("Check if all fields are there", async () => {
    render(
      <HashRouter>
        <LoginPage />
      </HashRouter>
    );
    expect(screen.getByText("Email address")).toBeInTheDocument();
    expect(screen.getByText("Password")).toBeInTheDocument();
    expect(screen.getByText("or continue with")).toBeInTheDocument();
    expect(screen.getByText("Sign in")).toBeInTheDocument();
  });
});
