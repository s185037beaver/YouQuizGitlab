import { HStack, Text } from "@chakra-ui/layout";
import { Link } from "react-router-dom";
import { FC } from "react";

const NavBar: FC = ({ children }) => {
  return (
    <nav>
      <HStack spacing={5}>
        <Link to={"/"}>
          <Text>Home</Text>
        </Link>
      </HStack>
    </nav>
  );
};

export default NavBar;
