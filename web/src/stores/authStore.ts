import { makeAutoObservable } from "mobx";
import { genUserClient } from "../services/apiClients";
import { UserStore } from "./userStore";
import jwtDecode from "jwt-decode";
import { UserModel } from "../model/user";

interface TokenPayload {
  user: UserModel;
  exp: number;
  issuer: string;
}

interface GoogleTokenPayload {
  email: string;
  name: string;
  exp: number;
}

export class AuthStoreImpl {
  private authenticated = false;

  // components that are "listening" to this AuthStore
  // will re-render because of the makeAutoObservable method called in the constructor.
  constructor() {
    makeAutoObservable(this);

    // Check for existing access-token
    this.checkAuth().then((res) => {
      this.setAuthenticated(res);
    });
  }

  async login(email: string, password: string) {
    const userClient = await genUserClient();
    const tokenPayload = await userClient.login(email, password);
    localStorage.setItem("access_token", tokenPayload);

    // decode JWT and set user
    const decodedToken = jwtDecode<TokenPayload>(tokenPayload);

    UserStore.setUser(decodedToken.user);
    this.setAuthenticated(true);
  }

  async googleLogin(token: string) {
    const userClient = await genUserClient();
    const tokenPayload = await userClient.googleLogin(token);
    localStorage.setItem("access_token", tokenPayload);

    const decodedToken = jwtDecode<TokenPayload>(tokenPayload);
    UserStore.setUser(decodedToken.user);
    console.log(decodedToken.user);
    this.setAuthenticated(true);
  }

  async signup(email: string, password: string) {
    const userClient = await genUserClient();
    const tokenPayload = await userClient.createUser(email, password);
    localStorage.setItem("access_token", tokenPayload);
    const decodedToken = jwtDecode<TokenPayload>(tokenPayload);
    UserStore.setUser(decodedToken.user);
    this.setAuthenticated(true);
  }

  async logout() {
    try {
      localStorage.setItem("access_token", "");
      this.setAuthenticated(false);

      // set user to null on logout
      UserStore.setUser(null);
    } catch (err) {
      throw new Error(err as string);
    }
  }

  async checkAuth() {
    // check for auth token in local storage
    if (!this.getAccessToken()) {
      this.setAuthenticated(false);
      return false;
    }

    try {
      // if a token exists - checks if the access token from local storage is valid
      const userClient = await genUserClient();
      const tokenPayload = await userClient.checkAuth();

      const decodedToken = jwtDecode<TokenPayload>(tokenPayload);
      UserStore.setUser(decodedToken.user);
      this.setAuthenticated(true);
      return true;
    } catch (error) {
      this.setAuthenticated(false);
      return false;
    }
  }

  private setAuthenticated(isAuthenticated: boolean) {
    this.authenticated = isAuthenticated;
  }

  getAccessToken() {
    return localStorage.getItem("access_token");
  }

  isAuthenticated() {
    return this.authenticated;
  }
}

export const AuthStore = new AuthStoreImpl();
