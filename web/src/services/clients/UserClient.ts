import { UserModel } from "../../model/user";
import { AuthBase, ClientBase } from "../clientBase";

export interface IUserClient {
  getUser: (id: number) => Promise<UserModel>;
  createUser: (email: string, password: string) => Promise<string>;
  updateUser: (id: number, user: UserModel) => Promise<UserModel>;
  deleteUser: (id: number) => Promise<void>;
  login: (email: string, password: string) => Promise<string>;
  logout: () => Promise<void>;
  checkAuth: () => Promise<string>;
  googleLogin: (token: string) => Promise<string>;
}

export class UserClient extends ClientBase implements IUserClient {
  private baseUrl?: string;

  constructor(configuration: AuthBase, baseUrl?: string) {
    super(configuration);
    // if client constructor is called without a baseUrl, localhost:8080 will be used
    this.baseUrl =
      baseUrl !== undefined && baseUrl !== null
        ? baseUrl
        : process.env.REACT_APP_API_URL;
  }

  getUser = async (id: number): Promise<UserModel> => {
    const url = `${this.baseUrl}/rest/user/${id}`;
    const options: RequestInit = {};

    return (
      this.transformOptions(options)
        .then((transformedOptions_) => fetch(url, transformedOptions_))
        .then((response: Response) => this.processResponse(response))
        // TODO handle the returned JSON object in next chain
        .then()
    );
  };

  login = async (email: string, password: string): Promise<string> => {
    const url = `${this.baseUrl}/rest/login`;
    const options: RequestInit = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    };

    return this.transformOptions(options)
      .then((transformedOptions_) => fetch(url, transformedOptions_))
      .then((response: Response) => this.processResponse(response))
      .then((res) => {
        return res as string;
      });
  };

  googleLogin = async (token: string): Promise<string> => {
    const url = `${this.baseUrl}/rest/login/google-login`;
    const options: RequestInit = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: token,
    };

    return this.transformOptions(options)
      .then((transformedOptions_) => fetch(url, transformedOptions_))
      .then((response: Response) => this.processResponse(response))
      .then((res) => {
        return res as string;
      });
  };

  logout = async (): Promise<void> => {
    return;
  };

  checkAuth = async (): Promise<string> => {
    const url = `${this.baseUrl}/rest/login/validate`;
    const options: RequestInit = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: localStorage.getItem("access_token") ?? "",
    };

    return this.transformOptions(options)
      .then((transformedOptions_) => fetch(url, transformedOptions_))
      .then((response: Response) => this.processResponse(response))
      .then((res) => {
        return res as string;
      });
  };

  createUser = async (email: string, password: string): Promise<string> => {
    const url = `${this.baseUrl}/rest/register`;
    const options: RequestInit = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    };
    return this.transformOptions(options)
      .then((transformedOptions_) => fetch(url, transformedOptions_))
      .then((response: Response) => this.processResponse(response))
      .then((res) => {
        return res as string;
      });
  };

  updateUser = async (id: number, user: UserModel): Promise<UserModel> => {
    const url = `${this.baseUrl}/rest/user`;
    const options: RequestInit = {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    };
    return (
      this.transformOptions(options)
        .then((transformedOptions_) => fetch(url, transformedOptions_))
        .then((response: Response) => this.processResponse(response))
        .catch((e) => console.log("ERROR", e))
        // TODO handle the returned JSON object in next chain
        .then()
    );
  };

  deleteUser = async (id: number): Promise<void> => {
    const url = `${this.baseUrl}/rest/user/${id}`;
    const options: RequestInit = {
      method: "DELETE",
    };
    return this.transformOptions(options)
      .then((transformedOptions_) => fetch(url, transformedOptions_))
      .then((response: Response) => this.processResponse<void>(response))
      .catch((e) => console.log("ERROR", e));
  };
}
