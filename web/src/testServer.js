import { rest } from "msw";
import { setupServer } from "msw/node";

const server = setupServer(
  rest.post(`${process.env.REACT_APP_API_URL}/rest/login`, (req, res, ctx) => {
    const { email, password } = req.body;

    if (email === "test@test.dk" && password === "1234") {
      return res(ctx.status(200), ctx.text("testToken"));
    } else {
      return res(ctx.status(401));
    }
  }),
  rest.get(`${process.env.REACT_APP_API_URL}/rest/quiz`, (req, res, ctx) => {
    return res(ctx.status(200));
  })
);

beforeAll(() => server.listen());
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

export { server, rest };
