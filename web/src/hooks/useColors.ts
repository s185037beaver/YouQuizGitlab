import { useColorModeValue } from "@chakra-ui/react";

export const useColors = () => {
  const headerBg = useColorModeValue("pink.300", "pink.800");
  const optionBg = useColorModeValue("pink.200", "pink.700");
  const optionBgHover = useColorModeValue("pink.300", "pink.800");
  const optionCorrect = useColorModeValue("green.400", "green.600");
  const optionIncorrect = useColorModeValue("red.400", "red.600");
  const primaryColorHover = useColorModeValue("pink.100", "pink.700");
  const quizItemBg = useColorModeValue("white", "pink.900");
  const quizItemBgHover = useColorModeValue("pink.100", "pink.700");

  return {
    headerBg,
    optionBg,
    optionBgHover,
    optionCorrect,
    optionIncorrect,
    primaryColorHover,
    quizItemBg,
    quizItemBgHover,
  };
};
