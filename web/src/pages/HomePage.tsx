import { Button } from "@chakra-ui/button";
import { Text } from "@chakra-ui/layout";
import { observer } from "mobx-react-lite";
import { FC } from "react";
import { Link } from "react-router-dom";
import BasicLayout from "../components/layouts/basicLayout";
import QuizSelection from "../components/quiz/QuizSelection/quizSelection";
import { AuthStore } from "../stores/authStore";

const Home: FC = () => {
  return (
    <BasicLayout>
      {AuthStore.isAuthenticated() ? (
        <QuizSelection />
      ) : (
        <>
          <Text>Welcome to YouQuiz! Please go to our login screen to continue 
            <Link to={"/login"}>
              <Button ml="20px" colorScheme="blue">LOGIN</Button>
            </Link>
          </Text>
        </>
      )}
    </BasicLayout>
  );
};

export default observer(Home);
