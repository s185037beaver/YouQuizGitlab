import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { HashRouter } from 'react-router-dom';
import { unmountComponentAtNode } from 'react-dom';
import { ChakraProvider } from '@chakra-ui/react';

test('App acutally renders', () => {
  const div = document.createElement("div");
  render(
  <HashRouter>
    <ChakraProvider>
      <App />
    </ChakraProvider>
  </HashRouter>, div
  );
  unmountComponentAtNode(div);
});
 