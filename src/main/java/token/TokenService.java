package token;


import User.dao.IUserDAO;
import User.dao.UserDAOImpl;
import User.dto.DBUserQuizzesDto;
import io.jsonwebtoken.Jwts;
import login.LoginData;
import login.User;
import User.DBUser;
import org.mindrot.jbcrypt.BCrypt;
import com.google.auth.oauth2.TokenVerifier;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;


@Path("login")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TokenService {

    private final IUserDAO userDAO = new UserDAOImpl();

    @POST
    public String postLoginData(LoginData login) throws NotAuthorizedException, NotFoundException {

        try {
            DBUserQuizzesDto user = userDAO.getUserByEmail(login.getEmail());

            if (user.getHashedPW().equals("GOOGLEOAUTH")) {
                throw new NotAuthorizedException("Login with google");
            }

            if (login != null && !user.getEmail().isEmpty() && BCrypt.checkpw(login.getPassword(), user.getHashedPW())) {
                return TokenHandler.generateJwtToken(new User(login.getEmail(), ""));
            }

            throw new NotAuthorizedException("wrong username/password");
        } catch (Exception e) {
            throw new NotFoundException(("Found no user with that email"));
        }

    }

    @POST
    @Path("validate")
    public Object postToken(String token) throws NotAuthorizedException {

        try {
            TokenHandler.validate(token);
            return token;
        } catch (NotAuthorizedException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

    }

    @POST
    @Path("google-login")
    public Object postGoogleLoginData(String token) {

        TokenVerifier tokenVerifier = TokenVerifier.newBuilder().build();
        try {
            // validates the google jwt token, throws exception if invalid
            tokenVerifier.verify(token);

            String email = decodeEmailFromGoogleJWT(token);

            try {
               userDAO.getUserByEmail(email).getEmail().equals(email);
            }catch(Exception e) {
                userDAO.addUser(new DBUser(email, "GOOGLEOAUTH"));
            }

            String ourJWT = TokenHandler.generateJwtToken(new User(email, token));

            return Response.status(Response.Status.OK).entity(ourJWT).build();
        } catch (TokenVerifier.VerificationException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

    }

    private String decodeEmailFromGoogleJWT(String token) {
        String[] chunks = token.split("\\.");

        Base64.Decoder decoder = Base64.getDecoder();

        String payload = new String(decoder.decode(chunks[1]));

        String[] contents = payload.split(",");

        String[] emailSplit = contents[4].split(":");

        String decodedEmail = emailSplit[1].replace("\"", "");

        return decodedEmail;
    }

}
