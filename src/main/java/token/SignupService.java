package token;

import User.dao.IUserDAO;
import User.dao.UserDAOImpl;
import User.dto.DBUserQuizzesDto;
import User.DBUser;
import login.LoginData;
import login.User;
import org.mindrot.jbcrypt.BCrypt;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("register")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SignupService {

    private final IUserDAO userDAO = new UserDAOImpl();

    @POST
    public String postLoginData(LoginData login) throws NotAuthorizedException {

        try {
            String hashedPass = BCrypt.hashpw(login.getPassword(), BCrypt.gensalt());

            try {
                if (userDAO.getUserByEmail(login.getEmail()).getEmail().equals(login.getEmail())) {
                    throw new NotAuthorizedException("A user with this email already exists");
                }
            } catch (Exception e) {
                if (e.getMessage().equals("User not found. email: " + login.getEmail())) {
                    userDAO.addUser(new DBUser(login.getEmail(), hashedPass));
                    return TokenHandler.generateJwtToken(new User(login.getEmail(), ""));
                }
            }

            return TokenHandler.generateJwtToken(new User(login.getEmail(), ""));

        } catch (Exception e) {
            e.printStackTrace();

            throw new NotAuthorizedException("A user with this email already exists");
        }
    }
}
