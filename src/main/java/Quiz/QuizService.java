package Quiz;

import Quiz.dao.IQuizDAO;
import Quiz.dao.QuizDaoImpl;
import Quiz.dto.CreateRandomQuizDto;
import Quiz.dto.QuizDto;
import Quiz.dto.QuizIdDto;
import Quiz.model.Quiz;
import QuizAPIClient.IQuizAPIClient;
import QuizAPIClient.QuizAPIClient;
import QuizAPIClient.QuizApiQuestionDto;
import User.DBUser;
import User.dao.IUserDAO;
import User.dao.UserDAOImpl;
import User.dto.DBUserQuizzesDto;
import Util.DTOUtil;
import Util.QuizAPIUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import token.SecureWithAuth;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import java.security.Principal;
import java.util.Collection;
import java.util.List;

@Path("quiz")
public class QuizService {

    private IQuizDAO quizDAO = new QuizDaoImpl();
    private IUserDAO userDAO = new UserDAOImpl();
    private IQuizAPIClient quizAPIClient = new QuizAPIClient();

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getQuiz(@PathParam("id") int id){
        QuizIdDto dto = quizDAO.getQuiz(id);
        return Response.status(Response.Status.OK).entity(dto).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllQuizes(){
        Collection<QuizIdDto> quizzes = quizDAO.getAllQuizzes();
        return Response.status(Response.Status.OK).entity(quizzes).build();
    }


    @POST
    @SecureWithAuth
    @Consumes("application/json")
    public Response createQuiz(QuizDto dto, @Context SecurityContext securityContext){
        Quiz quiz = DTOUtil.convert(dto, new TypeReference<Quiz>(){});

        Principal principal = securityContext.getUserPrincipal();
        String email = principal.getName();
        DBUserQuizzesDto user = userDAO.getUserByEmail(email);
        int id = quizDAO.addQuiz(quiz, user.getId());

        return Response.status(Response.Status.CREATED).entity(id).build();
    }

    @POST
    @SecureWithAuth
    @Path("random")
    @Consumes("application/json")
    public Response createRandomQuiz(CreateRandomQuizDto dto, @Context SecurityContext securityContext){
        try {
            List<QuizApiQuestionDto> apiQuestions = quizAPIClient.getQuiz(dto.getQuestionLimit(), dto.getCategory(), dto.getDifficulty());
            Quiz quiz = QuizAPIUtil.convertQuiz(apiQuestions);
            quiz.setTitle(dto.getTitle());
            quiz.setCategory(dto.getCategory().getCategory());
            quiz.setDescription(dto.getDescription());

            Principal principal = securityContext.getUserPrincipal();
            String email = principal.getName();
            DBUserQuizzesDto user = userDAO.getUserByEmail(email);
            int id = quizDAO.addQuiz(quiz, user.getId());

            return Response.status(Response.Status.CREATED).entity(id).build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e).build();
        }
    }

    @PUT
    @SecureWithAuth
    @Consumes("application/json")
    public Response updateQuiz(@QueryParam("id") int id, QuizIdDto dto, @Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        String email = principal.getName();
        DBUserQuizzesDto user = userDAO.getUserByEmail(email);

        QuizIdDto quiz = quizDAO.getQuiz(id);
        if (quiz.getCreatedById() != user.getId())
            return Response.status(Response.Status.UNAUTHORIZED).entity("You are not authorized to update this quiz.").build();

        Quiz newQuiz = DTOUtil.convert(dto, new TypeReference<Quiz>() {});
        quizDAO.updateQuiz(id, newQuiz);
        return Response.status(Response.Status.OK).entity("Quiz updated").build();
    }

    @DELETE
    @SecureWithAuth
    @Path("{id}")
    public Response deleteQuiz(@PathParam("id") int id, @Context SecurityContext securityContext) {
        Principal principal = securityContext.getUserPrincipal();
        String email = principal.getName();
        DBUserQuizzesDto user = userDAO.getUserByEmail(email);

        //Commented out for now, so that we can easily delete/clean up quizzes.
        /*
        QuizIdDto quiz = quizDAO.getQuiz(id);
        if (quiz.getCreatedById() != user.getId())
            return Response.status(Response.Status.UNAUTHORIZED).entity("You are not authorized to delete this quiz.").build();
        */

        quizDAO.deleteQuiz(id);
        return Response.status(Response.Status.OK).entity("Quiz deleted").build();
    }
}
