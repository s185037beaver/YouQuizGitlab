package login;

import lombok.Data;

public @Data class User {
    String email;
    String googleToken;

    public User() {

    }

    public User(String email, String googleToken) {
        this.email = email;
        this.googleToken = googleToken;
    }
}
